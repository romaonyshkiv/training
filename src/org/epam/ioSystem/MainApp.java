package org.epam.ioSystem;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class MainApp {


    public static void main(String... args) {

        List<Integer> myList = Arrays.asList(2,5,6,7,3,4,9,1);

//        myList.forEach(a -> System.out.println(a));

//        for (Integer data : myList) {
//            System.out.println(data);
//        }




        calculateAll(myList, a -> a > 4);

        calculateAll(myList, a -> a % 2 == 0);

//        int somm = 0;
//
//        for (Integer integer : myList) {
//            if (integer > 4) {
//                somm += integer;
//            }
//        }
//
//        System.out.println(somm);





//        File file = new File("src/org/epam/ioSystem/test.txt");
//
//        try {
//            BufferedReader br = new BufferedReader(new FileReader(file));
//            String text;
//            while ((text = br.readLine()) != null) {
//                System.out.println(text);
//            }
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        String textToBeWritten = "Write me in file";
//
//        BufferedWriter bw = null;
//        String fileToBeWritten = "src/org/epam/ioSystem/out.txt";
//
//        try {
//
//            FileWriter fileWriter = new FileWriter(fileToBeWritten);
//
//            bw = new BufferedWriter(fileWriter);


//            bw = new BufferedWriter(new FileWriter("src/org/epam/ioSystem/out.txt"));

//
//            bw.write(textToBeWritten);
//            bw.flush();
//
//
//        } catch (IOException e) {
//            e.printStackTrace();
//        } finally {
//            if (bw != null) {
//                try {
//                    bw.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//            }
//        }


//
//        Scanner scanner = null;
//        try {
//            scanner = new Scanner(file);
//
//            while (scanner.hasNext()) {
//                System.out.println(scanner.nextLine());
//            }
//
//        } catch (FileNotFoundException e) {
//            e.printStackTrace();
//        }

//        System.out.println("Enter text or 'Q' for exit");
//        String someText = scanner.nextLine();
//
//
//        while (!someText.equals("Q")) {
//            System.out.println("Text was " + someText);
//
//            System.out.println("Enter text or 'Q' for exit");
//            someText = scanner.nextLine();
//        }


    }


    public static void calculateAll(List<Integer> myList, MyLambda myLambda) {

        int summ = 0;

        for (Integer elementOfList : myList) {

            if (myLambda.check(elementOfList)) {

                summ += elementOfList;
            }
        }

        System.out.println(summ);

    }

}
