package org.epam.collection;

import java.util.*;

public class Main {

    public static void main(String... args) {

//        Set<String> mySet = new HashSet<>();
//
//        mySet.add("Steve");
//        mySet.add("Bob");
//        mySet.add("Ron");
//
//
//        for (String ob : mySet) {
//            System.out.println(ob);
//        }


//        Set<String> linkedSet = new LinkedHashSet<>();
//        linkedSet.add("Steve");
//        linkedSet.add("Bob");
//        linkedSet.add("Ron");
//
//
//        for (String ob : linkedSet) {
//            System.out.println(ob);
//        }


//        List<String> arrayList = new ArrayList<>();
//        arrayList.add("Steve");
//        arrayList.add("Bob");
//        arrayList.add("Ron");

//        for (String ob : arrayList) {
//            System.out.println(ob);
//        }


//        arrayList.add(1, "New guy");
//
//        System.out.println();
//
//        for (String ob : arrayList) {
//            System.out.println(ob);
//        }


//        List<String> linkedList = new LinkedList<>();
//        linkedList.add("Steve");
//        linkedList.add("Bob");
//        linkedList.add("Ron");
//
//
//        for (String ob : linkedList) {
//            System.out.println(ob);
//        }
//
//        linkedList.add(2, "new Guy");
//
//        System.out.println();
//
//        for (String ob : linkedList) {
//            System.out.println(ob);
//        }




//        Map<String, String> hashMap = new HashMap<>();
//
//        hashMap.put("Red" , "Car");
//        hashMap.put("Black", "Bike");
//        hashMap.put("Green", "Bus");
//
//        for (String ob : hashMap.keySet()) {
//            System.out.println(ob);
//        }
//
//        System.out.println();
//        for (String ob: hashMap.values()) {
//            System.out.println(ob);
//        }
//
//
//        System.out.println();
//
//        System.out.println(hashMap.get("Black"));



//
//        Map<String, String> hashTable = new Hashtable<>();
//
//        hashTable.put("Red" , "Car");
//        hashTable.put("Black", "Bike");
//        hashTable.put("Green", "Bus");
//
//
//        for (String ob : hashTable.keySet()) {
//            System.out.println(ob);
//        }
//
//        System.out.println();
//
//        System.out.println(hashTable.get("Red"));


//        List<String> names = new ArrayList<>();
//        names.add("Steve");
//        names.add("Bob");
//
//        List<String> colors = new ArrayList<>();
//        colors.add("Red");
//        colors.add("Black");
//
//
//
//        Map<String, Object> myMap = new HashMap<>();
//
//        myMap.put("Names", names);
//        myMap.put("Colors", colors);
//
//
//        System.out.println(myMap.get("Names"));


//        Person steve = new Person("Steve", 55544);
//        Person bob = new Person("Bob", 8787786);
//
//        List<Person> personList = new ArrayList<>();
//        personList.add(steve);
//        personList.add(bob);
//
//
//        for (Person person : personList) {
//            System.out.println(person);
//        }



//        Division division = new Division();
//
//
//        int i = 0;
//
//        while (i < 4) {
//            System.out.println(division.division(4, 0));
//            i++;
//        }
//
//
//


        try {
            int i = 9;

            switch (i) {
                case 1 :
                    System.out.println("i = " + i);
                    break;
                case 2 :
                    System.out.println("i = " + i);
                    break;
                default:
                    throw new MyException("Not implemented for " + i);
            }
        } catch (MyException e) {
            e.getMessage();
        }

//        int i = 9;
//
//        switch (i) {
//            case 1 :
//                System.out.println("i = " + i);
//                break;
//            case 2 :
//                System.out.println("i = " + i);
//                break;
//                default:
//                    throw new MyException("Not imp");
//        }


    }






}
