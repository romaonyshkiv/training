package org.epam.collection;

public class MyException extends Exception {

    public MyException(String msg) {
        super(msg);
    }
}
