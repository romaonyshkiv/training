package org.epam.constr;

public class Child extends Person {

    private int age;

    public Child(String name, String lastName, int age) {

        super(name, lastName);

        this.age = age;

    }

    public Child(String name, String lastName) {

        super(name, lastName);


    }




    public int getAge() {
        return age;
    }


    @Override
    public String toString() {
        return "Child{" + super.toString() + " age=" + age +  '}';
    }


    public class Child2 {

        private String address;

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }


        @Override
        public String toString() {
            return "Child2{" +
                    "address='" + address + '\'' +
                    '}';
        }
    }


}
