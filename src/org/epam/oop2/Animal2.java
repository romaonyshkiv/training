package org.epam.oop2;

public class Animal2 {


    private String name;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }




    public void action1() {
        System.out.println("public method in Animal2 " + name);
    }

    protected void action2() {
        System.out.println("protected method in Animal2 " + name);
    }

    private void action3() {
        System.out.println("private method in Animal2 " + name);
    }

    void action4() {
        System.out.println("default method in Animal2 " + name);
    }

}
