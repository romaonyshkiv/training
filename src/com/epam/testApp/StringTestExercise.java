package com.epam.testApp;


public class StringTestExercise {

    public static void main(String[] args) {

        String sPopularTopic_1 = "Selenium, Automation,Framework";
        String sPopularTopic_2 = "Basic Java Tutorial";

//        boolean bCompareResult = sPopularTopic_1.equals(sPopularTopic_2);
//        System.out.println("The result of String Comparison is : " + bCompareResult);

//        char cIndex = sPopularTopic_1.charAt(5);
//        System.out.println("The fifth character of Popular Topic 1 is : " + cIndex);
//
//        boolean bContainResult = sPopularTopic_1.contains("Framework");
//        System.out.println("The result of string Framework is contained in the String sPopularTopic_1 is : " + bContainResult);
//
//        int iIndex = sPopularTopic_1.indexOf("Framework");
//        System.out.println("The start index of the string Framework is : " + iIndex);
//
//        String sSubString = sPopularTopic_1.substring(15);
//        System.out.println("The sub string from the index number is : " + sSubString);
//
//        sSubString = sPopularTopic_1.substring(8, 19);
//        System.out.println("The sub string of Popular Topic 1 from index 8 to 18 is : " + sSubString);
//
//        String sLowerCase = sPopularTopic_1.toUpperCase();
//        System.out.println("The lower case of the Popular Topic 1 is : " + sLowerCase);
//
//        String [] aSplit = sPopularTopic_1.split(",");
//        System.out.println("The first part of the array is : " + aSplit[0]);
//        System.out.println("The last part of the array is : " + aSplit[1]);
//        System.out.println("The last part of the array is : " + aSplit[2]);


        StringBuilder builder = new StringBuilder();

        String result = sPopularTopic_1 + sPopularTopic_2;

        result = result + " some aditional text";


        builder.append(sPopularTopic_1);
        builder.append(sPopularTopic_2);
        builder.append("some aditional text");


        System.out.println(builder.toString());


    }

}